<!DOCTYPE HTML>
<html lang="fa-ir" dir="rtl"  data-config='{"twitter":0,"plusone":0,"facebook":0,"style":"default"}'>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body class="tm-sidebar-a-right tm-sidebars-1 tm-isblog">




<?php

try {

	$url = "http://www.sitepoint.com/forums/showthread.php?101526-Separating-internal-links-from-external-links";
	$base_url = parse_url($url, PHP_URL_HOST);
	$base_scheme = parse_url($url , PHP_URL_SCHEME);
	$base_scheme = $base_scheme ? $base_scheme : 'http';


	$html = file_get_contents($url);

	$doc = new DOMDocument();
	$doc->loadHTML($html);

	$links = [];
	$arr = $doc->getElementsByTagName("a");


	foreach($arr as $item){
		$href =  $item->getAttribute("href");

		if( empty(parse_url($href, PHP_URL_HOST))){
	    	$href = $base_scheme . '://' . $base_url . '/' . $href;
	    }

		$text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
		$links[] = array(
		    'href' => $href,
		    'text' => $text
	    );

	    echo '<a href="' . $href . '">' . $text . '</a><br/>';
	   
	    echo '<hr/>';
	}



}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}



function is_external_link($link = "http://abc.com/link1.html" , $base_url = "abc.com" , $base_scheme = 'http'){

	$link = parse_url($link, PHP_URL_HOST);
	$schema = parse_url($link, PHP_URL_SCHEME);

	if($schema !== $base_scheme){
		return true;
	}


	if(($link === $base_url) || empty($link))
	{
	   return false;
	}
	else
	{

	  	return true;
	}
}



function remove_http_s_www($url){
	return preg_replace('#^(http(s)?://)?w{3}\.#', '$1', $url);
}

?>





</body>
</html>